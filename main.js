const listDirectory = () => {
    function errorCallback(error)
    {
        document.getElementById('result').innerText = `An error occurred, during directory listing: ${error.message}`;
    }

    function successCallback(files, path)
    {
        document.getElementById('result').innerText = `Found directories in ${path} directory:`;
        for (var i = 0; i < files.length; i++)
        {
            document.getElementById('result').innerHTML = `<p>${files[i]}</p>`;
        }
    }
}

const createFolder = () => {
    function errorCallback(error)
    {
        document.getElementById('result').innerText = `An error occurred, during directory creation: ${error.message}`;
    }

    function successCallback(path)
    {
        document.getElementById('result').innerText = `The directory has been created, path to created directory: ${path}`;
        /* Directory can now be accessed. */
    }
}

const deleteFolder = () => {
    function errorCallback(error)
    {
       document.getElementById('result').innerText = `An error occurred, during directory deletion: ${error.message}`;
    }

    function successCallback(path)
    {
       document.getElementById('result').innerText = `The directory has been deleted, path to the parent of deleted directory: ${path}`;
    }
}

const createFile = (fileName) => {
    /** Case 0ne */
    try {
        var newFile = dir.createFile(fileName);
    } catch (error) {
        console.log(error)
    }

    /** Case two */
    function errorCallback(error)
    {
        document.getElementById('result').innerText = `An error occurred, during file deletion: ${error.message}`;
    }

    function successCallback(path)
    {
        document.getElementById('result').innerText = `The file has been deleted, path to the parent of deleted file: ${path}`;
    }

    try
    {
        tizen.filesystem.createFile(fileName, successCallback, errorCallback);
    }
    catch (error)
    {
        document.getElementById('result').innerText = `File cannot be created: ${error.message}`;
    }
}

const deleteFile = () => {
    function errorCallback(error)
    {
        document.getElementById('result').innerText = `An error occurred, during file deletion: ${error.message}`;
    }

    function successCallback(path)
    {
        document.getElementById('result').innerText = `The file has been deleted, path to the parent of deleted file: ${path}`;
    }
}

const writeFile = () => {}

const readFile = () => {}